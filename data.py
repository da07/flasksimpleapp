def Articles():
    articles = [
        {
            'id': 1,
            'title': 'article one ',
            'body': 'bla',
            'create_date': '01-09-2018'
        },
        {
            'id': 2,
            'title': 'article two ',
            'body': 'bla bla',
            'create_date': '02-09-2018'
        },
        {
            'id': 3,
            'title': 'article three ',
            'body': 'bla bla bla',
            'create_date': '03-09-2018'
        }

    ]
    return articles
